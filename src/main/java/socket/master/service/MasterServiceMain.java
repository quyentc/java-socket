package socket.master.service;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.List;

import socket.model.SharedFile;

public class MasterServiceMain {

	public static void main(String[] args) throws IOException, InterruptedException {

		HashMap<String, List<SharedFile>> listFile = new HashMap<String, List<SharedFile>>();

		// Serving file server
		ServingFileServerThread serverFile = new ServingFileServerThread();
		serverFile.start();

		// Client server
		int portClient = 8080;

		ServerSocket serverSocketClient = new ServerSocket(portClient);

		System.out.println("Server is listening Client on port " + portClient);
		while (true) {
			Socket socketClient = serverSocketClient.accept();
			System.out.println("New Client connected");
			listFile = serverFile.getListFile();
			MasterThreadClient thClient = new MasterThreadClient(socketClient, listFile);
			thClient.start();
		}
	}
}
