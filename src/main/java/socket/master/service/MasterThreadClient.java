package socket.master.service;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.HashMap;
import java.util.List;

import socket.model.SharedFile;

public class MasterThreadClient extends Thread {
	private Socket socket;
	private HashMap<String, List<SharedFile>> listFile = new HashMap<String, List<SharedFile>>();

	public MasterThreadClient(Socket socket, HashMap<String, List<SharedFile>> listFile) {
		this.socket = socket;
		this.listFile = listFile;
	}

	@Override
	public void run() {
		ObjectOutputStream objectOutput;
		try {
			objectOutput = new ObjectOutputStream(socket.getOutputStream());
			objectOutput.writeObject(this.listFile);
			objectOutput.flush();
			objectOutput.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
}
