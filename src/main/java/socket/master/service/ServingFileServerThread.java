package socket.master.service;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import socket.model.SharedFile;

public class ServingFileServerThread extends Thread {
	private int portFileServer = 8181;
	private ServerSocket serverSocketFile;
	private HashMap<String, List<SharedFile>> listFile = new HashMap<String, List<SharedFile>>();

	@Override
	public void run() {
		try {
			serverSocketFile = new ServerSocket(portFileServer);
			while (true) {
				System.out.println("Server is listening File server on port " + portFileServer);
				Socket socketFile = serverSocketFile.accept();
				System.out.println("New File server connected: " + socketFile.toString());
				ExecutorService executor = Executors.newCachedThreadPool();
				Future<HashMap<String, List<SharedFile>>> futureCall = executor
						.submit(new FileServerThread(socketFile));
				try {
					HashMap<String, List<SharedFile>> list = futureCall.get();
					listFile.putAll(list);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ExecutionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public HashMap<String, List<SharedFile>> getListFile() {
		return this.listFile;
	}
}
