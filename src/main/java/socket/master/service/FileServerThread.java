package socket.master.service;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Callable;

import socket.model.SharedFile;

public class FileServerThread implements Callable<HashMap<String, List<SharedFile>>> {

	private Socket socket;
	private HashMap<String, List<SharedFile>> listFile = new HashMap<String, List<SharedFile>>();

	public FileServerThread(Socket socket) {
		this.socket = socket;
	}

	public HashMap<String, List<SharedFile>> call() throws Exception {
		int port = socket.getPort();
		String key = Integer.toString(port);
		try {
			ObjectInputStream ois = new ObjectInputStream(this.socket.getInputStream());
			List<SharedFile> list = (List<SharedFile>) ois.readObject();
			listFile.put(key, list);
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return listFile;
	}

	// @Override
	// public void run() {
	// int port = socket.getPort();
	// String key = Integer.toString(port);
	// try {
	// ObjectInputStream ois = new ObjectInputStream(this.socket.getInputStream());
	// List<SharedFile> list = (List<SharedFile>) ois.readObject();
	// listFile.put(key, list);
	// } catch (IOException e1) {
	// e1.printStackTrace();
	// } catch (ClassNotFoundException e) {
	// e.printStackTrace();
	// }
	// }
}
