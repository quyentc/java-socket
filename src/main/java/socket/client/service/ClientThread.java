package socket.client.service;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;

import socket.model.FileInfo;

public class ClientThread extends Thread {
	private static final int PIECES_OF_FILE_SIZE = 1024 * 32;
	private DatagramSocket clientSocket;
	private String serverHost;
	private int port;
	private String fileName;
	private String pathSave;

	public ClientThread(int port, String serverHost, String fileName, String pathSave) {
		this.serverHost = serverHost;
		this.port = port;
		this.fileName = fileName;
		this.pathSave = pathSave;
	}

	private void connectServer() {
		try {
			clientSocket = new DatagramSocket();
		} catch (SocketException e) {
			e.printStackTrace();
		}
	}

	public void run() {
		connectServer();
		byte[] receiveData = new byte[PIECES_OF_FILE_SIZE];
		DatagramPacket receivePacket;
		DatagramPacket sendPacket;
		try {
			byte[] sentBuf = new byte[100];
			String sentMessage = this.fileName;

			InetAddress sentHost = InetAddress.getByName(this.serverHost);

			sentBuf = sentMessage.getBytes("ascii");
			sendPacket = new DatagramPacket(sentBuf, sentBuf.length, sentHost, this.port);
			clientSocket.send(sendPacket);

			receivePacket = new DatagramPacket(receiveData, receiveData.length);
			clientSocket.receive(receivePacket);
			InetAddress inetAddress = receivePacket.getAddress();
			ByteArrayInputStream byteArrInputStream = new ByteArrayInputStream(receivePacket.getData());
			ObjectInputStream objInputStream = new ObjectInputStream(byteArrInputStream);
			FileInfo fileInfo = (FileInfo) objInputStream.readObject();

			File fileReceive = new File(this.pathSave + "/" + fileInfo.getName());
			BufferedOutputStream bufferOutput = new BufferedOutputStream(new FileOutputStream(fileReceive));
			int pieces = 1;
			for (int i = 0; i < (fileInfo.getPiecesOfFile() - 1); i++) {
				receivePacket = new DatagramPacket(receiveData, receiveData.length, inetAddress, this.port);
				clientSocket.receive(receivePacket);
				bufferOutput.write(receiveData, 0, PIECES_OF_FILE_SIZE);
				System.out.printf("Downloading %s %.2f %%\n", fileInfo.getName(),
						+((double) pieces++ / fileInfo.getPiecesOfFile()) * 100);
			}
			receivePacket = new DatagramPacket(receiveData, receiveData.length, inetAddress, port);
			clientSocket.receive(receivePacket);
			bufferOutput.write(receiveData, 0, fileInfo.getLastByteLength());
			System.out.printf("Downloading %s %.2f %%\n", fileInfo.getName(),
					+((double) pieces / fileInfo.getPiecesOfFile()) * 100);
			bufferOutput.flush();
			bufferOutput.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

	}
}
