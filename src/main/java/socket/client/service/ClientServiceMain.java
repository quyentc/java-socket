package socket.client.service;

import java.awt.EventQueue;
import java.awt.Font;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.Vector;
import java.util.Map.Entry;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileSystemView;
import javax.swing.table.DefaultTableModel;

import org.apache.commons.io.FilenameUtils;

import socket.model.SharedFile;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ClientServiceMain extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String FILE_CONFIG = "/client/config/client.properties";
	private JPanel contentPane;
	private JTable tableFile;
	private DefaultTableModel modelTb = new DefaultTableModel(0, 0);;
	private Socket clientSocket;
	private HashMap<String, List<SharedFile>> hmShareFile = new HashMap<String, List<SharedFile>>();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ClientServiceMain frame = new ClientServiceMain();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public void connectMasterSever() throws ClassNotFoundException {
		InputStream configStream = ClientServiceMain.class.getResourceAsStream(FILE_CONFIG);
		Properties prop = new Properties();
		try {
			prop.load(configStream);
			String ip = prop.getProperty("master_server_ip");
			int port = Integer.parseInt(prop.getProperty("master_server_port"));
			clientSocket = new Socket(ip, port);

			InputStream inputStream = clientSocket.getInputStream();
			ObjectInputStream objectInput = new ObjectInputStream(inputStream);
			do {
				Object objectItem = objectInput.readObject();
				hmShareFile = (HashMap<String, List<SharedFile>>) objectItem;
			} while (true);

		} catch (Exception e) {
			// e.printStackTrace();
		} finally {
			try {
				if (configStream != null) {
					configStream.close();
				}
			} catch (IOException e) {
				// e.printStackTrace();
			}
		}
	}

	public void loadData() {
		modelTb.setRowCount(0);
		for (Entry<String, List<SharedFile>> e : hmShareFile.entrySet()) {
			List<SharedFile> lstFile = e.getValue();
			for (SharedFile sharedFile : lstFile) {
				Object[] data = { sharedFile.getFileName(), sharedFile.getSize(), sharedFile.getServer_ip(),
						sharedFile.getServer_port() };
				modelTb.addRow(data);
			}
		}
	}

	/**
	 * Create the frame.
	 */
	public ClientServiceMain() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1024, 768);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("Danh S\u00E1ch File Download");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNewLabel.setBounds(10, 10, 990, 40);
		contentPane.add(lblNewLabel);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 78, 990, 578);
		contentPane.add(scrollPane);

		String arrColumn[] = { "File Name", "File Size", "IP", "Port" };
		modelTb.setColumnIdentifiers(arrColumn);

		tableFile = new JTable();
		tableFile.setModel(modelTb);
		tableFile.getTableHeader().setReorderingAllowed(false);
		tableFile.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane.setViewportView(tableFile);

		JButton btnDownload = new JButton("Download");
		btnDownload.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					int selection = tableFile.getSelectedRow();
					if (selection == -1) {
						JOptionPane.showMessageDialog(null, "Select file download");
						return;
					}
					Vector<Object> rowData = modelTb.getDataVector().elementAt(selection);
					int port = (int) rowData.get(3);
					String fileName = (String) rowData.get(0);
					String serverHost = (String) rowData.get(2);
					JFileChooser btnFileChoose = new JFileChooser(
							FileSystemView.getFileSystemView().getHomeDirectory());
					File savedFile = new File(fileName);
					btnFileChoose.setSelectedFile(savedFile);
					int rVal = btnFileChoose.showSaveDialog(ClientServiceMain.this);
					if (rVal == JFileChooser.APPROVE_OPTION) {
						File fileSave = btnFileChoose.getCurrentDirectory();
						String pathSave = fileSave.getPath();
						ClientThread threadDownload = new ClientThread(port, serverHost, fileName, pathSave);
						threadDownload.start();
					}
				} catch (Exception err) {
					// TODO: handle exception
				}
			}
		});
		btnDownload.setBounds(10, 680, 120, 41);
		contentPane.add(btnDownload);
		try {
			connectMasterSever();
			loadData();
		} catch (ClassNotFoundException err) {
			// TODO Auto-generated catch block
			// err.printStackTrace();
		}
	}

}
