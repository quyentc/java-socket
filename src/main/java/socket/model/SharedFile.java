package socket.model;

import java.io.Serializable;

public class SharedFile implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String fileName;
	String path;
	long size;
	String extension;
	String server_ip;
	int server_port;

	public SharedFile() {

	}

	public SharedFile(String fileName, String path, long size, String extension, String server_ip, int server_port) {
		super();
		this.fileName = fileName;
		this.path = path;
		this.size = size;
		this.extension = extension;
		this.server_ip = server_ip;
		this.server_port = server_port;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public String getServer_ip() {
		return server_ip;
	}

	public void setServer_ip(String server_ip) {
		this.server_ip = server_ip;
	}

	public int getServer_port() {
		return server_port;
	}

	public void setServer_port(int server_port) {
		this.server_port = server_port;
	}

	@Override
	public String toString() {
		return "SharedFile [fileName=" + fileName + ", path=" + path + ", size=" + size + ", extension=" + extension
				+ ", server_ip=" + server_ip + ", server_port=" + server_port + "]";
	}

}
