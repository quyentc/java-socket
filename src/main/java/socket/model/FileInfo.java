package socket.model;

import java.io.Serializable;

public class FileInfo implements Serializable {
	private String name;
	private long size;
	private int piecesOfFile;
	private int lastByteLength;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public int getPiecesOfFile() {
		return piecesOfFile;
	}

	public void setPiecesOfFile(int piecesOfFile) {
		this.piecesOfFile = piecesOfFile;
	}

	public int getLastByteLength() {
		return lastByteLength;
	}

	public void setLastByteLength(int lastByteLength) {
		this.lastByteLength = lastByteLength;
	}

	@Override
	public String toString() {
		return "FileInfo [name=" + name + ", size=" + size + ", piecesOfFile=" + piecesOfFile + ", lastByteLength="
				+ lastByteLength + "]";
	}

}
