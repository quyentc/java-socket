package socket.file.service;

public class FileThread extends Thread {

	int port;

	public FileThread() {

	}

	public FileThread(int port) {
		this.port = port;
	}

	@Override
	public void run() {
		FileService service = new FileService(port);
		System.out.println("Filethread " + service.toString() + " start successful");
		service.connectMasterServer();
		service.serveDownloadForClient();
	}

}
