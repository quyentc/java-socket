package socket.file.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class FileServerMain {

	private Properties prop = new Properties();
	private static final String propFileName = "/file/server/config/file.properties";

	public static void main(String[] args) {
		FileServerMain server = new FileServerMain();
		server.run();
	}

	public FileServerMain() {
		try {
			InputStream is = FileServerMain.class.getResourceAsStream(propFileName);
			if (is == null) {
				throw new FileNotFoundException("properties file: " + propFileName + " not found in class path.");
			} else {
				prop.load(is);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void run() {
		for (int i = 1; i <= 5; i++) {
			int port = Integer.parseInt(prop.getProperty("port_" + i));
			FileThread fileServer = new FileThread(port);
			fileServer.start();
		}
	}
}
