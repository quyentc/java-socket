package socket.file.service;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

import socket.model.FileInfo;

public class DownloadThread extends Thread {

	private static final int PIECES_OF_FILE_SIZE = 32768;
	private String filename;
	private int clientPort;
	private InetAddress clientAddress;
	private int port;
	private DatagramSocket downloadSocket;
	private String path;
	private File file;
	private BufferedInputStream bis;

	public DownloadThread(DatagramPacket receivedPacket, int port) {
		this.filename = new String(receivedPacket.getData()).trim();
		this.clientPort = receivedPacket.getPort();
		this.clientAddress = receivedPacket.getAddress();
		this.port = port;
	}

	@Override
	public void run() {
		try {
			downloadSocket = new DatagramSocket();

			// Read file
			path = "/localhost" + port + "/" + filename;
			file = new File(DownloadThread.class.getResource(path.trim()).getPath());
			long fileSize = file.length();
			int piecesOfFile = (int) (fileSize / PIECES_OF_FILE_SIZE);
			int lastByteLength = (int) (fileSize % PIECES_OF_FILE_SIZE);
			if (lastByteLength > 0) {
				piecesOfFile++;
			}

			bis = new BufferedInputStream(new FileInputStream(file));
			byte[] bytePart = new byte[PIECES_OF_FILE_SIZE];
			byte[][] data = new byte[piecesOfFile][PIECES_OF_FILE_SIZE];
			int piece = 0;
			while (bis.read(bytePart, 0, PIECES_OF_FILE_SIZE) > 0) {
				data[piece++] = bytePart;
				bytePart = new byte[PIECES_OF_FILE_SIZE];
			}
			// Send FileInfo for client
			FileInfo fileInfo = new FileInfo();
			fileInfo.setName(filename);
			fileInfo.setSize(fileSize);
			fileInfo.setPiecesOfFile(piecesOfFile);
			fileInfo.setLastByteLength(lastByteLength);

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(baos);
			oos.writeObject(fileInfo);
			DatagramPacket fileInfoPacket = new DatagramPacket(baos.toByteArray(), baos.toByteArray().length,
					clientAddress, clientPort);
			downloadSocket.send(fileInfoPacket);
			waitMillisecond(100);

			// Send data of File
			for (int i = 0; i < piecesOfFile - 1; i++) {
				DatagramPacket dataPacket = new DatagramPacket(data[i], PIECES_OF_FILE_SIZE, clientAddress, clientPort);
				downloadSocket.send(dataPacket);
				waitMillisecond(50);
			}
			// send rest bytes
			DatagramPacket dataPacket = new DatagramPacket(data[piecesOfFile - 1], PIECES_OF_FILE_SIZE, clientAddress,
					clientPort);
			downloadSocket.send(dataPacket);
			System.out.println(filename + " Done!");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void waitMillisecond(long millisecond) {
		try {
			Thread.sleep(millisecond);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
