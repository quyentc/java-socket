package socket.file.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.commons.io.FilenameUtils;

import socket.model.SharedFile;

public class FileService {

	List<SharedFile> sharedFileList = null;
	private Properties prop = new Properties();
	private static final String propFileName = "/file/server/config/file.properties";
	private File folder;
	private Socket socket;
	private String host_ip;
	private int host_port;
	private int port;
	private ObjectOutputStream objectWriter;
	private DatagramSocket udpSocket;

	public FileService() {

	}

	public FileService(int port) {
		this.port = port;
		folder = new File("./file_repo/localhost" + port);
	}

	public void connectMasterServer() {
		try {
			InputStream is = FileServerMain.class.getResourceAsStream(propFileName);
			if (is == null) {
				throw new FileNotFoundException("properties file: " + propFileName + " not found in class path.");
			} else {
				prop.load(is);
				host_ip = prop.getProperty("master_ip");
				host_port = Integer.parseInt(prop.getProperty("master_port"));
			}

			sharedFileList = getListSharedFile();

			socket = new Socket(host_ip, host_port);

			objectWriter = new ObjectOutputStream(socket.getOutputStream());
			objectWriter.writeObject(sharedFileList);
			objectWriter.flush();
			objectWriter.close();

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	private List<SharedFile> getListSharedFile() {
		ArrayList<SharedFile> resultList = new ArrayList<SharedFile>();
		if (folder != null && folder.exists()) {
			File[] fileList = folder.listFiles();
			for (File sharedFile : fileList) {
				String ext = FilenameUtils.getExtension(String.valueOf(sharedFile.getName()));
				SharedFile file = new SharedFile();
				file.setFileName(sharedFile.getName());
				file.setExtension(ext);
				file.setPath(sharedFile.getPath());
				file.setSize(sharedFile.length());
				file.setServer_ip("localhost");
				file.setServer_port(port);
				resultList.add(file);
			}
		}
		return resultList;
	}

	public void serveDownloadForClient() {
		try {
			udpSocket = new DatagramSocket(port);
			do {
				byte[] clientMsg = new byte[256];
				DatagramPacket receivedPacket = new DatagramPacket(clientMsg, clientMsg.length);
				System.out.println("File server " + "localhost:" + port + " waiting for serving client download...");
				udpSocket.receive(receivedPacket);
				System.out.println(
						"File server " + "localhost:" + port + " received: " + new String(receivedPacket.getData()));
				new DownloadThread(receivedPacket, port).start();
			} while (true);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
